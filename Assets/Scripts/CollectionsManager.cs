﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionsManager : MonoBehaviour {

    static private CollectionsManager instance;


    static public void SetActiveTrexPart(int index, bool value) {
        instance.collections[0].SetActivePart(index, value);

        if (instance.collections[0].isComplete)
            BadgesManager.ActiveBadge(0);
    }

    static public void SetActiveTriceratopsPart(int index, bool value) {
        instance.collections[1].SetActivePart(index, value);

        if (instance.collections[1].isComplete)
            BadgesManager.ActiveBadge(1);
    }

    static public void SetActiveVelociraptorPart(int index, bool value) {
        instance.collections[2].SetActivePart(index, value);

        if (instance.collections[2].isComplete)
            BadgesManager.ActiveBadge(2);
    }

    static public Museum ExportMuseum() {
        Museum museum = new Museum();

        museum.trex = instance.collections[0].Export();
        museum.triceratops = instance.collections[1].Export();
        museum.velociraptor = instance.collections[2].Export();

        return museum;
    }

    static public bool CheckCollectionByIndex(int index) {
        return instance.collections[index].isComplete;
    }




    private AudioSource audioSource;

    [SerializeField]
    private float alpha;

    [Range(0f, 10f)]
    public float radio = 5f;

    [Range(0f, 360f)]
    public float offset;
    public float speed = 1;
    public AnimationCurve curve;


    public Collection[] collections;



    public enum State {
        idle, moving
    }
    public State state = State.idle;



    void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();

        CollectionsController.UpdateDescription(SharedState.LanguageDefs["collectionstrex"]);
    }

    // Update is called once per frame
    void Update() {
        UpdateCollections();
    }

    void OnDrawGizmos() {
        UpdateCollections();

        foreach (Collection collection in collections)
            Debug.DrawLine(transform.position, collection.position);
    }


    private void UpdateCollections() {
        float interval = 360f / collections.Length;// * Mathf.Deg2Rad;
        float angle;
        Vector3 pos = Vector3.zero;

        for (int i = 0; i < collections.Length; i++) {
            angle = (interval * i) + offset + alpha;
            angle *= Mathf.Deg2Rad;

            pos.x = Mathf.Cos(angle) * radio;
            pos.y = 0;
            pos.z = Mathf.Sin(angle) * radio;
            pos += transform.position;

            collections[i].position = pos;
        }
    }


    private IEnumerator Move(float to) {
        state = State.moving;

        audioSource.Play();
        CollectionsController.HideDescription();

        float from = alpha;
        float t = 0;

        while (t < 1f) {
            alpha = Mathf.Lerp(from, to, curve.Evaluate(t));

            t += speed * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }


        alpha = to;

        if (alpha >= 360f)
            alpha -= 360f;

        if (alpha <= -360f)
            alpha = 360f;

        //string dino = alpha switch {
        //    0 => "trex",
        //    120 => "triceratops",
        //    240 => "velociraptor",
        //    _ => ""
        //};
        string dino = "";
        switch (alpha) {
            case 0:
                dino = "trex";
                break;

            case 120:
                dino = "velociraptor";
                break;

            case 240:
                dino = "triceratops";
                break;
        }

        CollectionsController.ShowDescription();
        CollectionsController.UpdateDescription(SharedState.LanguageDefs["collections"+dino]);
        state = State.idle;
    }


    public void Next() {
        if (state == State.moving)
            return;

        float interval = 360f / collections.Length;
        StartCoroutine(Move(alpha + interval));
    }

    public void Previous() {
        if (state == State.moving)
            return;

        float interval = 360f / collections.Length;
        StartCoroutine(Move(alpha - interval));
    }

}
