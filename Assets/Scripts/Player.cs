﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    static private Player instance;


    static public void StartAutoMove(int direction) {
        instance.AutoMove(direction);
    }

    static public void StopAutoMove() {
        instance.Stop();
    }

    static public void Lock() {
        instance.state = State.waiting;
    }

    static public void Unlock() {
        instance.state = State.idle;
    }

    static public void SetMask(int index = -1) {
        instance.maskController.SetActiveMaskByIndex(index);

        switch (index) {
            case 0:
                instance.currentStats = instance.trexStats;
                break;

            case 1:
                instance.currentStats = instance.triceratopsStats;
                break;

            case 2:
                instance.currentStats = instance.velociraptorStats;
                break;
        }

        instance.currentHP = instance.hp + (index == -1 ? 0 : instance.currentStats.life);
        HealthBarController.SetMaxValue(instance.currentHP);
        HealthBarController.SetValue(instance.currentHP);
        instance.damage = 1 + instance.currentStats.damage;

        instance.hitFx.SetActive(index == -1);
        instance.trexStats.hitFx.SetActive(index == 0);
        instance.triceratopsStats.hitFx.SetActive(index == 1);
        instance.velociraptorStats.hitFx.SetActive(index == 2);

    }


    static public State currentState {
        get { return instance.state; }
    }

    static public Vector3 position {
        get => instance.transform.position;
        set => instance.transform.position = value;
    }

    static public Transform parent {
        get => instance.transform.parent;
    }




    [System.Serializable]
    public struct PlayerSoundFx {
        public AudioClip walk;
        public AudioClip dig;
        public AudioClip attack;
        public AudioClip hit;
        public AudioClip death;
    }



    private Animator animator;
    private Rigidbody2D rb2D;
    private AudioSource audioSource;
    [SerializeField]
    private Vector2 velocity;
    private int direction = 1;
    [SerializeField]
    private int currentHP;

    private Layer layer;
    private bool volcanic;
    private int volcanicIndex = -1;


    public MaskController maskController;
    public float speed = 1f;
    public float digSpeed = 3f;

    public int damage = 1;
    public int hp = 9;
    //For jump
    public float force = 5f;
    [SerializeField]
    private bool canJump = false;
    public bool jump = false;
    [SerializeField]
    private bool isGrounded = true;


    public enum State {
        idle, walk, dig, waiting, attacking, falling, hit, jump
    }
    public State state;



    [Header("FX")]
    public PlayerSoundFx fxs;
    public GameObject hitFx;
    public ParticleSystem smokeFx;


    [System.Serializable]
    public struct Stats {
        public int damage;
        public int speed;
        public int life;
        public GameObject hitFx;
    }

    [Header("Stats")]
    private Stats currentStats;
    public Stats trexStats;
    public Stats triceratopsStats;
    public Stats velociraptorStats;




    void Awake() {
        instance = this;

        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

        direction = (int)transform.localScale.x;
        velocity = Vector2.zero;
        layer = null;

        state = State.idle;
    }

    // Start is called before the first frame update
    void Start() {
        smokeFx.Stop();

        currentHP = hp;
        HealthBarController.SetMaxValue(hp);
        HealthBarController.SetValue(hp);

        //For jumping
        rb2D = GetComponent<Rigidbody2D>();

        //animator = GetComponent<Animator>();
        //rb2D = GetComponent<Rigidbody2D>();
        //audioSource = GetComponent<AudioSource>();

        //direction = (int)transform.localScale.x;
        //velocity = Vector2.zero;
        //layer = null;

        //state = State.idle;
    }

    // Update is called once per frame
    void Update() {



        if (GameManager.currentState == GameManager.State.pause ||
        //GameManager.currentState == GameManager.State.intro || 
        GameManager.currentState == GameManager.State.finish) {
            animator.SetBool("isWalking", false);
            velocity = Vector3.zero;

            rb2D.velocity = Vector2.zero;

            audioSource.loop = false;
            audioSource.Stop();

            smokeFx.Stop();

            return;
        }





        if (state == State.waiting) {
            //Debug.Log("update state -> " + state);
            animator.SetBool("isWalking", false);
            //state = State.idle;
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);

            audioSource.loop = false;
            audioSource.Stop();

            smokeFx.Stop();

            return;
        }

        if (state == State.hit || state == State.falling) return;

        if (state != State.idle && state != State.walk && state != State.jump) {
            //Debug.Log("state -> " + state);
            animator.SetBool("isWalking", false);
            velocity = Vector3.zero;

            if (state == State.waiting) {
                audioSource.loop = false;
                audioSource.Stop();

                smokeFx.Stop();
            }
            return;
        }


        if (GameManager.currentState == GameManager.State.intro) return;
        //velocity.x = Input.GetAxisRaw("Horizontal") * speed;
        //if (Input.GetKeyDown(KeyCode.A)) {
        //    velocity.x = -(speed + currentStats.speed);
        //    direction = 1;
        //}
        //else if (Input.GetKeyDown(KeyCode.D)) {
        //    velocity.x = (speed + currentStats.speed);
        //    direction = -1;
        //}

        float axiX = Input.GetAxisRaw("Horizontal");
        velocity.x = axiX * (speed + currentStats.speed);
        animator.SetFloat("speed", Mathf.Abs(axiX));
        if (velocity.x < 0) {
            direction = 1;
        }
        else if (velocity.x > 0) {
            direction = -1;
        }


        //float axiY = Input.GetAxisRaw("Vertical");
        //if (axiY > 0)
        //    LiftManager.Up();
        //else if (axiY < 0) {
        //    Debug.Log(state);
        //    LiftManager.Down();
        //}




        if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
            velocity.x = 0;
        else if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            velocity.x = 0;




        if (Input.GetMouseButtonDown(0)) {
            if (layer && isGrounded)
                Dig();
            else if (volcanic) {
                state = State.waiting;
                TestLayer();
            }
        }

        if (state != State.waiting) {
            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
                LiftManager.Up();
            else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                LiftManager.Down();
        }


        if (Input.GetMouseButtonDown(1))
            Attack();


        if (jump && Input.GetKeyDown(KeyCode.Space) && canJump && (state == State.idle || state == State.walk)) {
            Jump();
            state = State.jump;
        }


    }

    void FixedUpdate() {
        if (GameManager.currentState == GameManager.State.finish) {
            rb2D.velocity = Vector2.zero;
            rb2D.constraints = RigidbodyConstraints2D.FreezeAll;
            return;
        }

        if (state == State.waiting) {
            //Debug.Log("Fixed state -> " + state);
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
            return;
        }


        if (state == State.falling) {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
            return;
        }


        //rigidbody2D.position += position;
        rb2D.velocity = new Vector2(velocity.x, rb2D.velocity.y);

        if ((state != State.idle && state != State.walk) || state == State.jump)
            return;

        bool isWalking = rb2D.velocity.x != 0;
        animator.SetBool("isWalking", isWalking);
        //animator.SetBool("isFalling", false);


        //if (rb2D.velocity.y < 0) {
        //    animator.SetBool("isWalking", false);
        //    animator.SetBool("isFalling", true);
        //}


        state = isWalking ? State.walk : State.idle;

        if (isWalking) {
            if (!audioSource.isPlaying) {
                audioSource.clip = fxs.walk;
                audioSource.loop = true;
                audioSource.Play();
            }

            if (smokeFx.isStopped)
                smokeFx.Play();
        }
        else {
            if (audioSource.clip == fxs.walk) {
                audioSource.loop = false;
                audioSource.Stop();
            }

            smokeFx.Stop();
        }


        transform.localScale = new Vector3(direction, 1, 1);
        smokeFx.transform.localScale = transform.localScale;


    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Lift")) {
            //transform.parent = collision.transform;
            //Lift.Load();
            if (collision.collider.offset.y <= 0.25f) {
                //rb2D.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

                transform.position = new Vector3(transform.position.x, collision.transform.position.y + 0.655f);
                //Debug.Log("OnCollisionEnter2D");
                //state = State.idle;
                //Debug.Break();

                //canJump = true;
                isGrounded = true;

                if (state == State.jump)
                    state = State.idle;
            }


        }
        else if (collision.gameObject.CompareTag("Layer")) {
            layer = collision.gameObject.GetComponentInParent<Layer>();
        }
        else if (collision.gameObject.CompareTag("Volcanic")) {
            volcanic = true;
            volcanicIndex = collision.gameObject.GetComponent<Volcanic>().datingIndex;
        }
        else if (collision.gameObject.CompareTag("Floor")) {
            isGrounded = true;
            canJump = true;
            if (state != State.waiting)
                state = State.idle;
        }
    }

    void OnCollisionStay2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Lift")) {
            if (collision.collider.offset.y <= 0.25f) {
                //    rb2D.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

                transform.position = new Vector3(transform.position.x, collision.transform.position.y + 0.655f);
                //    //Debug.LogWarning("OnCollisionStay2D " + state + " " + Time.time);
                //    //state = State.idle;
                //    //Debug.Break();

                //if (state != State.waiting) {
                //    canJump = true;
                //    state = State.idle;
                //}
                if (jump)
                    canJump = false;
            }


        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Lift")) {
            //transform.parent = null;
            //Lift.Unload();
            //if (collision.collider.offset.y <= 0.25f) {
            //    rb2D.constraints = RigidbodyConstraints2D.FreezeRotation;

            //    transform.position = new Vector3(transform.position.x, collision.transform.position.y + 0.655f);
            //    //Debug.Log("OnCollisionExit2D");
            //    //state = State.idle;
            //    //Debug.Break();
            //}
        }
        else if (collision.gameObject.CompareTag("Layer")) {
            layer = null;
        }
        else if (collision.gameObject.CompareTag("Volcanic")) {
            volcanic = false;
            volcanicIndex = -1;
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Coin")) {
            Item item = collision.GetComponent<Item>();
            item.Destroy();
            GameManager.ScoreAdd(1);
        }


        if (collision.CompareTag("Item")) {
            Item item = collision.GetComponent<Item>();
            GameManager.AddItem(item.Data());
            GameManager.AddTime(Level1_1Manager.extraTime, collision.transform.position);
            item.Destroy();
        }

        if (collision.CompareTag("Cure")) {
            Cure(3);
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Collection")) {
            GameManager.AddCollection(collision.GetComponent<CollectionPart>().id);
            GameManager.AddTime(Level1_1Manager.extraTimeCollection, collision.transform.position);
            Destroy(collision.gameObject);
        }


        if (collision.name == "hit") {
            Hit(collision.GetComponentInParent<Enemy>().damage);
        }

        if (collision.CompareTag("DeathZone")) {
            Hit(currentHP);
        }

        if (collision.CompareTag("Tunnel")) {
            if (transform.position.y >= collision.transform.position.y + collision.offset.y) {
                state = State.falling;
                animator.SetBool("isFalling", true);
                StartCoroutine(FallingAling(collision.transform.position.x));
            }
        }



        if (collision.gameObject.CompareTag("Volcanic")) {
            //volcanic = true;
            volcanicIndex = collision.gameObject.GetComponent<Volcanic>().datingIndex;
            TestLayer();
        }

    }




    private IEnumerator FallingAling(float x) {
        float startX = transform.position.x;
        float endX = x;
        float t = 0;

        audioSource.loop = false;
        audioSource.Stop();

        smokeFx.Stop();


        while (t < 1f) {
            transform.position = new Vector3(
                Mathf.Lerp(startX, endX, t),
                transform.position.y,
                0);

            t += Time.deltaTime * 2f;

            yield return new WaitForEndOfFrame();
        }

        while (true) {
            transform.position = new Vector3(
                endX,
                transform.position.y,
                0);

            yield return new WaitForEndOfFrame();
        }

    }


    private IEnumerator DigAnimation(float duration) {
        state = State.dig;

        audioSource.clip = fxs.dig;
        audioSource.loop = false;
        audioSource.Play();

        layer.Dig(transform.position.x - layer.transform.position.x > 0);
        animator.SetFloat("testing", 0);
        animator.SetBool("isDigging", true);

        yield return new WaitForSeconds(duration);

        animator.SetBool("isDigging", false);

        if (state != State.waiting)
            state = State.idle;
    }



    public void Dig() {
        if (layer == null)
            return;


        StartCoroutine(DigAnimation(.75f));
    }

    public void TestLayer() {
        //Debug.Log("test layer");

        state = State.waiting;

        animator.SetFloat("testing", 1f);
        animator.SetBool("isDigging", true);

        GameManager.PauseTimer();
    }

    public void OnTestingLayerEnd() {
        Lock(true);
        GameManager.ShowDatingPanel(volcanicIndex);
    }


    public void Attack() {
        state = State.attacking;
        animator.SetTrigger("attack");

        audioSource.clip = fxs.attack;
        audioSource.loop = false;
        audioSource.Play();
    }

    public void Jump() {
        animator.SetTrigger("jump");
        rb2D.AddForce(Vector2.up * force, ForceMode2D.Impulse);

        audioSource.loop = false;
        audioSource.Stop();

        smokeFx.Stop();

        isGrounded = false;
        canJump = false;
    }

    public void OnAttackEnd() {
        if (state != State.waiting)
            state = State.idle;
    }

    public void Hit(int value) {
        currentHP -= value;
        currentHP = Mathf.Max(currentHP, 0);


        HealthBarController.SetValue(currentHP);
        animator.SetTrigger("hit");
        state = State.hit;

        audioSource.clip = fxs.hit;
        audioSource.loop = false;
        audioSource.Play();


        if (currentHP <= 0) {
            audioSource.clip = fxs.death;
            audioSource.loop = false;
            audioSource.Play();

            GameManager.GameOver();
        }


    }

    public void OnHitEnd() {
        state = State.idle;
    }

    public void Cure(int value) {
        currentHP += value;
        currentHP = Mathf.Min(currentHP, 9);

        HealthBarController.SetValue(currentHP);
    }

    public void Lock(bool value) {
        if (value)
            state = State.waiting;
        else {
            state = State.idle;
            animator.SetBool("isDigging", false);
        }

    }

    public void AutoMove(int direction = 1) {
        velocity.x = (speed + currentStats.speed) * direction;
        this.direction = -direction;

        state = State.walk;
    }

    public void Stop() {
        velocity.x = 0;

        state = State.idle;
    }
}
