﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuController : MonoBehaviour {

    public TextMeshProUGUI[] texts;


    // Start is called before the first frame update
    void Start() {
        UpdateTexts();
    }

    // Update is called once per frame
    void Update() {

    }

    public void LoadLevelByIndex(int index) {
        SceneManager.LoadScene(index);
    }

    public void UpdateTexts() {
        foreach (TextMeshProUGUI text in texts) {
            string[] cmds = text.text.Split('+');
            //text.text = SharedState.LanguageDefs["mainMenu"][cmds[0]] + (cmds.Length == 2? cmds[1].Replace("\"", "") : "");
            text.text = SharedState.LanguageDefs["mainMenu"+cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
        }

    }
}
