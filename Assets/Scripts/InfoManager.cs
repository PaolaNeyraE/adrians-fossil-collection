﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoManager : MonoBehaviour {

    public GameObject[] panels;

    public GameObject btnClose;


    public bool isActive {
        get {
            int count = 0;

            foreach (GameObject panel in panels)
                if (panel.activeSelf)
                    count++;

            return count > 0;
        }
    }


    // Start is called before the first frame update
    void Start() {
        //btnClose.SetActive(false); 
        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }

    public void SetActive(bool value) {
        foreach (GameObject panel in panels)
            panel.SetActive(value);
    }

    public void EnableClose() {
        btnClose.SetActive(true);
    }

    public void ShowPanel(int index) {
        SetActive(false);
        panels[index].SetActive(true);
    }

}
