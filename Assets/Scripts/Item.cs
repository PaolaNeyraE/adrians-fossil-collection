﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public int id = -1;
    public AudioClip clip;

    public Sprite sprite {
        get {
            return GetComponent<SpriteRenderer>().sprite;
        }
    }


    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }

    void OnDestroy() {
        
    }

    public Item Clone() {
        return (Item)this.MemberwiseClone();
    }

    public ItemData Data() {
        //return new ItemData(id);
        //Debug.Log("ID - " + id);
        return ItemsDB.GetItemDataById(id);
    }

    public void Destroy() {
        GameObject fx = new GameObject("FX");
        AudioSource audio = fx.AddComponent<AudioSource>();
        audio.clip = clip;
        audio.playOnAwake = false;
        audio.loop = false;
        audio.Play();

        Destroy(fx, clip.length);
        Destroy(gameObject);
    }
}

[System.Serializable]
public struct ItemData {
    public int id;
    public int stack;
    public string name;
    public string info1;
    public string info2;
    public Sprite sprite;

    public ItemData(int id, Sprite sprite = null, int stack = 1, 
        string name = "", string info1 = "", string info2 = "") {
        this.id = id;
        this.stack = stack;
        this.name = name;
        this.info1 = info1;
        this.info2 = info2;
        this.sprite = sprite;
    }
}
