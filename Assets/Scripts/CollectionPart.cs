﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionPart : MonoBehaviour {

    public string id;
    public Chest chest;
    public Vector3 offset;
    public AudioClip clip;


    public delegate void OnPick();
    public event OnPick onPick;


    // Start is called before the first frame update
    void Start() {
        GetComponent<CircleCollider2D>().enabled = false;

        Chest clone = Instantiate(chest, transform.position + offset, Quaternion.identity);
        clone.transform.localScale = new Vector3(Mathf.Sign(transform.position.x - Player.position.x), 1f, 1f);
        clone.openEnd += () => {
            GetComponent<CircleCollider2D>().enabled = true;
        };

        //transform.SetParent(clone.glow, false);
        transform.parent = clone.glow;
        transform.localPosition = Vector3.zero;
        transform.localScale = -Vector3.one;



        onPick += () => { Destroy(clone.gameObject); };
    }

    // Update is called once per frame
    void Update() {

    }

    void OnDestroy() {
        GameObject fx = new GameObject("FX");
        AudioSource audio = fx.AddComponent<AudioSource>();
        audio.clip = clip;
        audio.playOnAwake = false;
        audio.loop = false;
        audio.Play();

        Destroy(fx, clip.length);
        onPick?.Invoke();
    }
}
