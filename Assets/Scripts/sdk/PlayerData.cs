﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData {
    public int coins = 0;
    public int last_level_index = 1;
    public int max_level_index = 1;

    public List<Fossil> fossils;
    public Museum museum;
}

[System.Serializable]
public class Fossil {
    public int item_id = -1;
    public int stack = 0;
}

[System.Serializable]
public class Museum {
    public List<int> badges;
    public bool[] velociraptor;
    public bool[] triceratops;
    public bool[] trex;
}