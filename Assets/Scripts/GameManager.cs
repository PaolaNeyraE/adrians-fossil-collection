﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour {

    static private GameManager instance;

    static public void ScoreAdd(int value) {
        Level1_1Manager.coins++;
        instance.AddScore(value);
    }

    static public void AddItem(ItemData item) {
        Level1_1Manager.fossils++;
        instance.inventoryController.AddItem(item);
    }

    static public void AddCollection(string id) {
        Level1_1Manager.collections++;

        int index = (int.Parse(id.Substring(1))) - 1;

        switch (id[0]) {
            case '0':
                CollectionsManager.SetActiveTrexPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(0))
                    BadgeController.Show(0);

                break;

            case '1':
                CollectionsManager.SetActiveTriceratopsPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(1))
                    BadgeController.Show(1);

                break;

            case '2':
                CollectionsManager.SetActiveVelociraptorPart(index, true);

                if (CollectionsManager.CheckCollectionByIndex(2))
                    BadgeController.Show(2);

                break;
        }


    }

    static public void StopTimer() {
        instance.timer.Stop();
    }

    static public void PauseTimer() {
        instance.timer.Pause();
    }

    static public void PlayTimer() {
        instance.timer.Play();
    }

    static public void AddTime(int time, Vector3 position) {
        instance.timer.AddTime(time);
        TextFx clone = Instantiate(instance.textFxPrefab, position , Quaternion.identity);
        clone.value = time;
    }


    static public void PlayGame() {
        instance.state = State.game;
    }

    static public void PauseGame() {
        instance.state = State.pause;
    }

    static public void FinishGame() {
        instance.state = State.finish;
    }

    static public void TimesUp() {
        instance.state = State.finish;

        instance.timesUpController.SetActive(true);
    }

    static public void GameOver() {
        instance.state = State.finish;

        instance.gameOverPanel.SetActive(true);
        StopTimer();
    }

    static public void ShowDatingPanel(int index) {
        //instance.datingPanel[index].SetActive(true);
        instance.datingPanel[index].Show();
    }

    static public void PlayBGMain() {
        instance.audioSource.Stop();
        instance.audioSource.clip = instance.main;
        instance.audioSource.Play();
    }

    static public void PlayBGOther() {
        instance.audioSource.Stop();
        instance.audioSource.clip = instance.other;
        instance.audioSource.loop = true;
        instance.audioSource.Play();
    }

    static public int time {
        get => instance.timer.timeInSeconds;
    }

    static public string timeFormated {
        get => instance.timer.timeFormated;
    }

    static public int currentScore {
        get => instance.score;
    }

    static public State currentState {
        get => instance.state;
    }







    public enum State {
        intro, game, pause, finish
    }
    public State state = State.intro;



    public PlayerData playerData;


    [Header("Score")]
    public int score;
    public TextMeshProUGUI txtScore;


    [Header("Timer")]
    public Timer timer;


    [Header("Inventory")]
    public InventoryController inventoryController;

    [Header("Pause Menu")]
    public PauseMenuController pauseMenuController;


    [Header("Level info")]
    public LevelInfoController levelInfoController;

    [Header("Time¡s up")]
    public TimesUpController timesUpController;

    [Header("Game Over")]
    public GameOverPanel gameOverPanel;


    [Header("Dating")]
    public DatingPanel[] datingPanel;


    [Header("Audio BG")]
    private AudioSource audioSource;
    public AudioClip main;
    public AudioClip other;


    [Header("FX")]
    public TextFx textFxPrefab;


    void Awake() {
        instance = this;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = false;
    }


    // Start is called before the first frame update
    void Start() {

        Time.timeScale = 1;
        UpdateScore();

        //timer.Play();
        Load();
    }

    // Update is called once per frame
    void Update() {
        if (state == State.intro || state == State.finish)
            return;

        if (Input.GetKeyDown(KeyCode.I)) {
            if (!inventoryController.isOpen)
                OpenInventory();

            else
                CloseInventory();
        }

    }


    public void GoToMainMenu() {
        SceneManager.LoadScene(0);
    }

    public void GoToLevel(int index) {
        SceneManager.LoadScene(index);
    }


    public void OpenInventory() {
        state = State.pause;

        timer.Pause();
        inventoryController.SetActive(true);
    }

    public void CloseInventory() {
        state = State.game;

        timer.Play();
        inventoryController.SetActive(false);
    }

    public void AddScore(int value) {
        score += value;

        UpdateScore();
    }

    public void UpdateScore() {
        txtScore.text = score.ToString("000");
    }

    public void Play() {
        state = State.game;
    }

    public void Pause() {
        Debug.Log("GameManager -> Pause");
        state = State.pause;


        Time.timeScale = 0;

        pauseMenuController.SetActive(true);
        pauseMenuController.SetScore(score);
        pauseMenuController.SetTime(timer.timeFormated);
    }

    public void Continue() {
        Debug.Log("GameManager -> Continue");
        state = State.game;


        Time.timeScale = 1;

        pauseMenuController.SetActive(false);
    }

    public void Finish() {
        state = State.finish;
    }

    public void ShowLevelInfo() {
        state = State.pause;

        Time.timeScale = 0;

        levelInfoController.SetActive(true);
    }

    public void HideLevelInfo() {
        state = State.game;

        Time.timeScale = 1;

        levelInfoController.SetActive(false);
    }


    public void Save() {
        GameLoader.Save();
    }

    public void Load() {
        GameLoader.Load(state => {
            GameLoader.SetCurrentLevelIndex(SceneManager.GetActiveScene().buildIndex);

            //GameManager.ScoreAdd(state.coins);
            AddScore(state.coins);

            state.fossils.ForEach(fossil => {
                ItemData item = ItemsDB.GetItemDataById(fossil.item_id);
                item.stack = fossil.stack;
                GameManager.AddItem(item);
            });


            for (int i = 0; i < state.museum.trex.Length; i++)
                CollectionsManager.SetActiveTrexPart(i, state.museum.trex[i]);

            for (int i = 0; i < state.museum.triceratops.Length; i++)
                CollectionsManager.SetActiveTriceratopsPart(i, state.museum.triceratops[i]);

            for (int i = 0; i < state.museum.velociraptor.Length; i++)
                CollectionsManager.SetActiveVelociraptorPart(i, state.museum.velociraptor[i]);


            //Debug.Log(SharedState.LanguageDefs["somePanel"]["text001"]);

        });
    }

}
