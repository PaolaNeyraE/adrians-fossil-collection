﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour {



    private enum State {
        empty, loaded, moving
    }
    [SerializeField]
    private State state = State.empty;

    public bool isMoving {
        get => state == State.moving;
    }


    [SerializeField]
    private int currentLevel = 0;


    public float speed = 1f;
    public Transform[] levels;


    public Animator gateAnimator;
    public GateController gateController;

    public Lift other;

    public AudioClip clip;
    private AudioSource audioSource;

    public float minDistance = 1.4f;


    // Start is called before the first frame update
    void Start() {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        audioSource.clip = clip;
    }

    //// Update is called once per frame
    //void LateUpdate() {
    //    if (state != State.loaded)
    //        return;

    //    if (Player.currentState != Player.State.waiting) {
    //        Debug.Log(Player.currentState);
    //        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) {
    //            UpLevel();
    //            other?.UpLevel();
    //        }
    //        else if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) {
    //            Debug.Log(name + " -> " + Player.currentState);
    //            DownLevel();
    //            other?.DownLevel();
    //        }
    //    }


    //}

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.transform.parent = transform;
            LiftManager.Load(this);
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            collision.transform.parent = null;
            LiftManager.Unload();
        }
    }



    //private IEnumerator Move(int from, int to) {
    //    float t = 0;
    //    state = State.moving;

    //    audioSource.Play();

    //    //gateAnimator.SetBool("isOpen", false);

    //    if (Player.parent == transform) {
    //        Player.Lock();

    //        while (Vector3.Distance(transform.position, Player.position) > minDistance) {
    //            Player.StartAutoMove((int)Mathf.Sign(transform.position.x - Player.position.x));
    //            yield return new WaitForEndOfFrame();
    //        }

    //        Player.StopAutoMove();
    //    }


    //    gateController.Close();
    //    yield return new WaitForSeconds(.3f);

    //    while (t < 1f) {
    //        transform.position = Vector3.Lerp(levels[from].position, levels[to].position, t);
    //        t += speed * Time.deltaTime;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    transform.position = levels[to].position;

    //    //gateAnimator.SetBool("isOpen", true);
    //    gateController.Open();
    //    yield return new WaitForSeconds(.3f);

    //    if (Player.parent == transform)
    //        if (Player.currentState != Player.State.waiting)
    //            Player.Unlock();

    //    audioSource.Stop();

    //    currentLevel = to;
    //    state = State.loaded;
    //}

    //public void UpLevel() {
    //    if (currentLevel <= 0)
    //        return;

    //    StartCoroutine(Move(currentLevel, currentLevel - 1));
    //}

    //public void DownLevel() {
    //    if (currentLevel >= levels.Length - 1)
    //        return;

    //    StartCoroutine(Move(currentLevel, currentLevel + 1));
    //}

    //public void Load() {
    //    if (state == State.empty)
    //        state = State.loaded;
    //}

    //public void Unload() {
    //    if (state == State.loaded)
    //        state = State.empty;
    //}

    public void PlayFx() {
        audioSource.Play();
    }

    public void StopFx() {
        audioSource.Stop();
    }

    public void PauseFx() {
        audioSource.Pause();
    }

    public void UnPauseFx() {
        audioSource.UnPause();
    }
}
