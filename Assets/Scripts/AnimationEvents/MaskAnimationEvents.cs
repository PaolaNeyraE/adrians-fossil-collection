﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskAnimationEvents : MonoBehaviour {


    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }


    public void EmptyEvent() { }

    public void OnAnimationEnd() {
        gameObject.SetActive(false);
    }
}
