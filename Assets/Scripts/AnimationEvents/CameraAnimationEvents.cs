﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimationEvents : MonoBehaviour {

    [System.Serializable]
    public struct Layer {
        public GameObject layer;
        public GameObject[] patches;

        public void SetActive(bool active) {
            layer.SetActive(active);

            foreach (GameObject patch in patches)
                patch.SetActive(active);
        }
    }

    public Layer[] layers;

    public GameObject[] labels;

    public bool intro = true;

    // Start is called before the first frame update
    void Start() {
        //foreach (Layer layer in layers)
        //    layer.SetActive(false);
        ShowAllLayers(!intro);

        //if (!intro)
        //    GetComponent<Animator>().SetTrigger("zoom_in");

        HideLabels();
    }

    // Update is called once per frame
    void Update() {

    }




    public void ShowLabel(int index) {
        if (index >= 0 & index < labels.Length)
            labels[index].SetActive(true);
    }

    public void HideLabels() {
        if (labels == null) return;

        foreach (GameObject label in labels)
            label.SetActive(false);
    }

    public void ShowLayer(int index) {
        if (index >= 0 && index < layers.Length)
            layers[index].SetActive(true);
    }

    public void ShowAllLayers() {
        ShowAllLayers(true);
    }

    public void ShowAllLayers(bool value) {
        foreach (Layer layer in layers)
            layer.SetActive(value);
    }


    public void FinishZoomIn() {
        Destroy(GetComponent<Animator>());
        CameraFollow.StartFollow();
    }

}
