﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationEvents : MonoBehaviour {

    [SerializeField]
    private SpriteRenderer[] sprites;

    private ParticleSystem particle;
    public Enemy enemy;

    // Start is called before the first frame update
    void Start() {
        sprites = GetComponentsInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnDigStart() {
        //Debug.Log("OnDigStart " + sprites.Length);
        foreach (SpriteRenderer sprite in sprites)
            sprite.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;

        particle = Instantiate(enemy.particle, enemy.transform.position, enemy.particle.transform.rotation);
    }

    public void OnDigEnd() {
        //Debug.Log("OnDigEnd " + sprites.Length);
        foreach (SpriteRenderer sprite in sprites) {
            sprite.maskInteraction = SpriteMaskInteraction.None;
            //Debug.Log(sprite.name + " " + sprite.maskInteraction);
        }

        enemy.Idle();

        particle.Stop();
        Destroy(particle, 0.25f);
    }

    public void OnDeadEnd() {
        enemy.Die();
    }


    public void OnAttackStart() {
        enemy.Attack();
    }

    public void OnAttackEnd() {
        enemy.Idle();
    }

    public void OnHitEnd() {
        enemy.HitEnd();
    }


}
