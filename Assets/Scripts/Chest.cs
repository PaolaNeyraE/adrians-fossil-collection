﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {

    private AudioSource audioSource;


    public Transform glow;
    public CollectionPart collectionPart;


    public delegate void OpenEnd();
    public event OpenEnd openEnd;

    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnOpenEnd() {
        openEnd?.Invoke();
    }

    public void PlayFX() {
        audioSource.Play();
    }
}
