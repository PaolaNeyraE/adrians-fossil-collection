﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDev : MonoBehaviour {
#if UNITY_EDITOR

    public Inventory inventory;
    public ItemData item;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            inventory.AddItem(item);
    }
#endif
}
