﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer : MonoBehaviour {

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private BoxCollider2D boxCollider;
    [SerializeField] private Animator animator;


    public delegate void OnDigEnd(Layer tile, int direction);
    public OnDigEnd onDigEnd;

    public delegate void OnDigStart(Layer tile, int direction);
    public OnDigStart onDigStart;


    private ParticleSystem particleClone;
    public ParticleSystem particle;
    public ParticleSystem enemyParticle;
    public GameObject loot;



    // Start is called before the first frame update
    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {

    }

    void OnDrawGizmos() {
        if (loot == null)
            return;


        if (loot.GetComponent<CollectionPart>()) {

            switch (loot.GetComponent<CollectionPart>().id[0]) {
                case '0':
                    Gizmos.DrawIcon(transform.position, "trex.png", false);
                    break;

                case '1':
                    Gizmos.DrawIcon(transform.position, "triceratops.png", false);
                    break;

                case '2':
                    Gizmos.DrawIcon(transform.position, "velociraptor.png", false);
                    break;
            }


            return;
        }

        if (loot.CompareTag("Cure")) {
            Gizmos.DrawIcon(transform.position, "cure.png", false);
            return;
        }


        switch (loot.name) {
            case "SpiderSpawner":
                Gizmos.DrawIcon(transform.position, "spider.png", false);
                break;

            case "SpiderSpawner_roof":
                Gizmos.DrawIcon(transform.position, "spider_roof.png", false);
                break;

            case "ScorpionSpawner":
                Gizmos.DrawIcon(transform.position, "scorpion.png", false);
                break;

            case "BeetleSpawner":
                Gizmos.DrawIcon(transform.position, "beetle.png", false);
                break;

            default:
                if (loot.GetComponent<Item>())
                    Gizmos.DrawIcon(transform.position, "fossil.png", false);
                break;
        }
    }




    private void Destroy() {
        if (loot != null) {
            GameObject clone = Instantiate(loot, transform.position, Quaternion.identity);
            EnemySpawner spawner = clone.GetComponent<EnemySpawner>();

            if (spawner)
                spawner.particle = enemyParticle;
        }

        Destroy(gameObject);
    }


    public void DigStart() {
        particleClone = Instantiate(particle, transform.position, particle.transform.rotation);
        particleClone.transform.localScale = new Vector3(spriteRenderer.flipX ? 1 : -1, 1f, 1f);

        onDigStart?.Invoke(this, spriteRenderer.flipX ? 1 : -1);
    }

    public void Dig(bool flipX = false) {
        //DigStart();

        spriteRenderer.flipX = flipX;
        //Destroy(boxCollider);

        animator.SetBool("dig", true);
    }

    public void DigEnd() {
        particleClone.Stop();
        Destroy(particleClone.gameObject, 2f);

        onDigEnd?.Invoke(this, spriteRenderer.flipX ? 1 : -1);
        Destroy();
    }

}
