﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class Mission : MonoBehaviour {

    private AudioSource audioSource;
    

    public UnityEvent onShow;
    public UnityEvent onComplete;


    public TextMeshProUGUI txtMision;


    public string description {
        get => txtMision.text;
    }


    private string _key;
    public string key {
        get => _key;
    }


    void Awake() {
        _key = txtMision.text;
    }

    void Start() {
        audioSource = GetComponent<AudioSource>();    
    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);
    }

    public void Show() {
        Player.Lock();
        Enemy.isLocked = true;
        GameManager.PauseTimer();
        SetActive(true);

        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        if (audioSource != null)
            audioSource?.Play();

        onShow?.Invoke();
    }

    public void Hide() {
        Player.Unlock();
        Enemy.isLocked = false;
        GameManager.PlayTimer();
        SetActive(false);

        onComplete?.Invoke();
    }

    
}
