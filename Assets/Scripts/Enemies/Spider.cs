﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy {

    private float originalSpeed;
    private bool isRunning;

    public float runSpeedMultiplier;
    public float viewRnge = 50f;
    public LayerMask playerMask; 



    private void LateUpdate() {
        Vector2 origin = transform.position + Vector3.right + transform.up;
        Vector2 direction = transform.right * Mathf.Sign(this.direction);

        RaycastHit2D hit = Physics2D.Raycast(origin, direction, viewRnge, playerMask);

        if (hit.collider != null) {
            if (hit.collider.CompareTag("Player")) {
                Debug.DrawLine(origin, hit.point, Color.green);

                if (!isRunning) {
                    isRunning = true;

                    originalSpeed = speed;
                    speed *= runSpeedMultiplier;
                }

            }
            else {
                Debug.DrawLine(origin, origin + (direction * viewRnge), Color.red);

                if (isRunning) {
                    isRunning = false;

                    speed = originalSpeed;
                }

            }
        }
        else {
            Debug.DrawLine(origin, origin + (direction * viewRnge), Color.red);

            if (isRunning) {
                isRunning = false;

                speed = originalSpeed;
            }

        }
    }
}
