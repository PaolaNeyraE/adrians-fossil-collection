﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public delegate void SpawnEnemy(Enemy enemy);
    static public event SpawnEnemy OnSpawn;

    public ParticleSystem particle;

    public Enemy enemy;
    public float offsetY;

    // Start is called before the first frame update
    void Start() {
        Spawn();
    }

    // Update is called once per frame
    void Update() {

    }



    public void Spawn() {
        Vector3 pos = transform.position;
        pos.y += offsetY;

        Enemy clone = Instantiate(enemy, pos, enemy.transform.rotation);
        clone.direction = Mathf.Sign(Player.position.x - pos.x);
        clone.particle = particle;

        OnSpawn?.Invoke(clone);

        Destroy(gameObject);
    }



}
