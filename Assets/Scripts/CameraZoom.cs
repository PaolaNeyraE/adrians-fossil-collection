﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {

    private Camera mainCamera;

    public float speed = 3f;
    public float minSize = 3.2f;
    public float maxSize = 16f;


    // Start is called before the first frame update
    void Start() {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update() {
        float s = -Input.GetAxis("Mouse ScrollWheel");

        if (Input.GetKey(KeyCode.Q))
            s = -0.08f;
        else if (Input.GetKey(KeyCode.E))
            s = 0.08f;

        s *= speed * 100f * Time.deltaTime;

        float size = mainCamera.orthographicSize + s;
        size = Mathf.Clamp(size, minSize, maxSize);

        mainCamera.orthographicSize = size;
    }
}
