﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    static private CameraFollow instance;

    static public void StartFollow() {
        instance.state = State.follow;
    }


    [SerializeField]
    private Camera mainCamera;


    public Transform target;

    [Range(0f, 1f)]
    public float offsetX;

    [Range(0f, 1f)]
    public float offsetY;



    public float smooth = 1f;
    public Vector2 minLimit;
    public Vector2 maxLimit;


    public enum State {
        idle, follow
    }
    public State state = State.idle;

    // Start is called before the first frame update
    void Start() {
        instance = this;
    }

    // Update is called once per frame
    void Update() {

    }

    void LateUpdate() {
        if (target == null)
            return;

        if (state == State.idle)
            return;


        float distanceX = target.position.x - transform.position.x;
        float distanceY = target.position.y - transform.position.y;
        Vector3 pos = Vector3.zero;

        Vector3 offset = new Vector3(offsetX, offsetY);
        offset.x *= mainCamera.orthographicSize * 2f * mainCamera.aspect;
        offset.y *= mainCamera.orthographicSize;

        if (Mathf.Abs(distanceX) > (offset.x) * 0.5f)
            pos.x = distanceX;

        if (Mathf.Abs(distanceY) > (offset.y) * 0.5f)
            pos.y = distanceY;


        pos *= smooth * Time.deltaTime;
        pos += transform.position;

        pos.x = Mathf.Clamp(pos.x,
            minLimit.x + mainCamera.orthographicSize * mainCamera.aspect,
            maxLimit.x - mainCamera.orthographicSize * mainCamera.aspect);


        pos.y = Mathf.Clamp(pos.y,
            minLimit.y + mainCamera.orthographicSize,
            maxLimit.y - mainCamera.orthographicSize);

        //if (pos.x - (mainCamera.orthographicSize * mainCamera.aspect) > minLimit.x)
        transform.position = pos;

    }

    void OnDrawGizmosSelected() {
        Vector3 offset = new Vector3(offsetX, offsetY);
        //offset.x *= 32 * mainCamera.aspect;
        //offset.y *= 16;
        offset.x *= mainCamera.orthographicSize * 2f * mainCamera.aspect;
        offset.y *= mainCamera.orthographicSize;
        Gizmos.DrawWireCube(transform.position, offset);


        Vector3 bottomLimit = transform.position;
        bottomLimit.x -= mainCamera.orthographicSize * mainCamera.aspect;
        bottomLimit.y -= mainCamera.orthographicSize;

        Gizmos.DrawCube(bottomLimit, Vector3.one * 1f);
        


        Vector3 vector = Vector3.zero;
        vector.x = minLimit.x;
        Gizmos.color = Color.red;
        Gizmos.DrawCube(vector, Vector3.one * 1f);
        vector.x += mainCamera.orthographicSize * mainCamera.aspect;
        Gizmos.color = Color.magenta;
        Gizmos.DrawCube(vector, Vector3.one * 1f);

        vector = Vector3.zero;
        vector.x = maxLimit.x;
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(vector, Vector3.one * 1f);
        vector.x -= mainCamera.orthographicSize * mainCamera.aspect;
        Gizmos.color = Color.cyan;
        Gizmos.DrawCube(vector, Vector3.one * 1f);
    }
}
