﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CustomEvents : MonoBehaviour {
    [System.Serializable]
    public struct TriggerEnter2D {
        public string tag;
        public UnityEvent eventHandle;
    }

    [System.Serializable]
    public struct GetKeyDown {
        public KeyCode KeyCode;
        public UnityEvent eventHandle;
    }



    public TriggerEnter2D[] onTriggerEnter2D;
    public GetKeyDown[] onGetKeyDown;


    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        foreach (GetKeyDown getKeyDown in onGetKeyDown)
            if (Input.GetKeyDown(getKeyDown.KeyCode))
                getKeyDown.eventHandle?.Invoke();
    }

    void OnTriggerEnter2D(Collider2D collision) {
        foreach (TriggerEnter2D triggerEnter2D in onTriggerEnter2D)
            if (collision.CompareTag(triggerEnter2D.tag))
                triggerEnter2D.eventHandle?.Invoke();
    }
}
