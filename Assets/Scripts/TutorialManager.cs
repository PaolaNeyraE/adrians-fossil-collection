﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialManager : MonoBehaviour {

    [System.Serializable]
    public struct Tutorial {
        public GameObject panel;
        public UnityEvent onComplete;
    }

    public Tutorial[] tutorials;
    public GameObject[] triggers;


    // Start is called before the first frame update
    void Start() {
        foreach (Tutorial tutorial in tutorials)
            tutorial.panel.SetActive(false);

        foreach (GameObject trigger in triggers)
            trigger.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }


    public void CloseTutorial(int id) {
        tutorials[id].panel.SetActive(false);
        tutorials[id].onComplete?.Invoke();
        Player.Unlock();
        GameManager.PlayTimer();
        LiftManager.Continue();
        Enemy.isLocked = false;
    }

    public void ShowTutorial(int id) {
        tutorials[id].panel.SetActive(true);
        GameManager.PauseTimer();
        Player.Lock();
        LiftManager.Pause();
        Enemy.isLocked = true;
    }


}
