﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadgesManager : MonoBehaviour {

    static private BadgesManager instance;

    static public void ActiveBadge(int index) {
        instance.badges[index].SetActive(true);
    }


    public GameObject[] badges;


    void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start() {
        foreach (GameObject badge in badges)
            badge.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }

}
