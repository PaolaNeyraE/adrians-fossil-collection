﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimesUpController : MonoBehaviour {

    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();

        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            audioSource.Play();
    }

}
