﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseMenuController : MonoBehaviour {

    public TextMeshProUGUI txtScore;
    public TextMeshProUGUI txtTime;

    public GameObject controlsPanel;

    // Start is called before the first frame update
    void Start() {
        controlsPanel.SetActive(false);
        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGOther();
        else
            GameManager.PlayBGMain();
    }

    public void SetTime(string time) {
        txtTime.text = time;
    }

    public void SetScore(int score) {
        txtScore.text = score.ToString("000");
    }

}
