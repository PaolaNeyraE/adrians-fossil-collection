﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class ResumenController : MonoBehaviour {

    private Coroutine particleCoroutine;

    [SerializeField]
    private float totalCoins = 100;
    [SerializeField]
    private float totalFossils = 30;
    [SerializeField]
    private Museum museum;
    private bool[][] totalCollections = new bool[3][];


    public float delay = 1f;
    public RectTransform particleParent;
    public Image emptyParticle;
    public float particleRate = 1f;


    [Header("Coins")]
    public GameObject coinsGroup;
    public TextMeshProUGUI txtCoins;
    public Sprite[] coinIcons;
    public AudioSource audioCoins;

    [Header("Fossils")]
    public GameObject fossilsGroup;
    public TextMeshProUGUI txtFossils;
    public Sprite[] fossilIcons;
    public AudioSource audioFossils;

    [Header("Collections")]
    public GameObject collectionsGroup;
    public TextMeshProUGUI txtCollections;
    public Collections[] collections;
    public GameObject[] badges;
    public AnimationCurve curve;

    [System.Serializable]
    public struct Collections {
        public GameObject root;
        public GameObject[] bones;
    }



    [Header("Game Over")]
    public GameObject gameOverGroup;


    // Start is called before the first frame update
    void Start() {
        coinsGroup.SetActive(false);
        fossilsGroup.SetActive(false);
        collectionsGroup.SetActive(false);
        gameOverGroup.SetActive(false);

        Load();
        //totalCollections[0] = museum.trex;
        //totalCollections[1] = museum.triceratops;
        //totalCollections[2] = museum.velociraptor;

        //Invoke("ShowCoinsResume", delay);



        GameLoader.Complete();
    }

    // Update is called once per frame
    void Update() {

    }


    private void Load() {
        GameLoader.Load(state => {

            totalCoins = state.coins;

            state.fossils.ForEach((item) => totalFossils += item.stack);

            totalCollections[0] = state.museum.trex;
            totalCollections[1] = state.museum.triceratops;
            totalCollections[2] = state.museum.velociraptor;

            Invoke("ShowCoinsResume", delay);
        });
    }



    private IEnumerator EmitParticles(Sprite[] particleIcons) {
        Image clone;

        while (true) {
            clone = Instantiate(emptyParticle, particleParent);
            clone.sprite = particleIcons[Random.Range(0, particleIcons.Length)];
            clone.transform.localPosition = Vector3.right * Random.Range(-512f, 512f);

            yield return new WaitForSeconds(particleRate);
        }

    }

    private IEnumerator CountCoins() {
        float currentCoins = 0;

        txtCoins.text = currentCoins.ToString();
        yield return new WaitForSeconds(1f);

        audioCoins.Play();

        while (currentCoins <= totalCoins) {
            txtCoins.text = currentCoins.ToString();

            currentCoins++;

            yield return new WaitForEndOfFrame();
        }

        audioCoins.Stop();

        StopCoroutine(particleCoroutine);
        yield return new WaitForSeconds(2f);

        ShowFossilsResume();
    }

    private IEnumerator CountFossils() {
        float currentFossils = 0;

        txtFossils.text = currentFossils.ToString();
        yield return new WaitForSeconds(1f);


        audioFossils.Play();

        while (currentFossils <= totalFossils) {
            txtFossils.text = currentFossils.ToString();

            currentFossils++;

            yield return new WaitForEndOfFrame();
        }

        audioFossils.Stop();

        StopCoroutine(particleCoroutine);
        yield return new WaitForSeconds(2f);

        ShowCollectionsResume();
    }

    private IEnumerator CountCollections() {
        int currentCOllections = 0;

        yield return new WaitForSeconds(1.5f);

        for (int i = 0; i < totalCollections.Length; i++) {

            int count = 0;

            collections[i].root.SetActive(true);
            yield return new WaitForSeconds(1f);

            for (int j = 0; j < totalCollections[i].Length; j++) {
                if (totalCollections[i][j]) {
                    collections[i].bones[j].gameObject.SetActive(totalCollections[i][j]);
                    count++;

                    yield return new WaitForSeconds(.5f);
                }
            }

            if (count >= totalCollections[i].Length) {
                currentCOllections++;
                yield return CompleteCollection(i);
            }
        }

        txtCollections.text = currentCOllections.ToString() + "/3";
        txtCollections.gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);

        ShowGameOverResume();
    }

    private IEnumerator CompleteCollection(int index) {
        float t = 0;

        Destroy(collections[index].root.GetComponent<Animator>());

        while (t <= 1f) {
            collections[index].root.transform.localScale = Vector3.one * curve.Evaluate(t);
            t += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        collections[index].root.transform.localScale = Vector3.one * curve.Evaluate(1f);
        badges[index].SetActive(true);
    }



    public void ShowCoinsResume() {
        coinsGroup.SetActive(true);

        particleCoroutine = StartCoroutine(EmitParticles(coinIcons));
        StartCoroutine(CountCoins());
    }

    public void ShowFossilsResume() {
        coinsGroup.SetActive(false);
        fossilsGroup.SetActive(true);

        particleCoroutine = StartCoroutine(EmitParticles(fossilIcons));
        StartCoroutine(CountFossils());

    }

    public void ShowCollectionsResume() {
        coinsGroup.SetActive(false);
        fossilsGroup.SetActive(false);
        collectionsGroup.SetActive(true);

        txtCollections.gameObject.SetActive(false);

        foreach (Collections collection in collections) {
            foreach (GameObject bone in collection.bones)
                bone.SetActive(false);

            collection.root.SetActive(false);
        }

        foreach (GameObject badge in badges)
            badge.SetActive(false);

        StartCoroutine(CountCollections());
    }

    public void ShowGameOverResume() {
        coinsGroup.SetActive(false);
        fossilsGroup.SetActive(false);
        collectionsGroup.SetActive(false);
        gameOverGroup.SetActive(true);
    }

    public void GoToMainMenu() {
        SceneManager.LoadScene(0);
    }
}
