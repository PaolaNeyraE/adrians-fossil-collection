﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Question : MonoBehaviour {
    [SerializeField]
    private Button[] buttons;

    public delegate void Callback();

    public TextMeshProUGUI txtQuestion;
    public GameObject[] answers;

    public float introSpeed = 1f;
    public float outroSpeed = 1f;
    public float outroDelay = 1f;


    // Start is called before the first frame update
    void Awake() {
        buttons = new Button[answers.Length];
        for (int i = 0; i < answers.Length; i++)
            buttons[i] = answers[i].GetComponentInChildren<Button>();
    }

    // Update is called once per frame
    void Update() {
        
    }


    private void SetInteractable(bool value) {
        foreach (Button button in buttons)
            button.interactable = value;
    }

    private IEnumerator IntroQuestion() {
        SetInteractable(false);

        foreach (GameObject answer in answers)
            answer.transform.localScale = Vector3.zero;

        float t = 0;
        while (t < 1f) {
            foreach (GameObject answer in answers)
                answer.transform.localScale = Vector3.one * t;
            t += introSpeed * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        foreach (GameObject answer in answers)
            answer.transform.localScale = Vector3.one;

        SetInteractable(true);
    }

    private IEnumerator OutroQuestion(Callback callback) {
        Vector3[] startPositions = new Vector3[answers.Length];
        Vector3[] endPositions = new Vector3[answers.Length];

        for (int i = 0; i < answers.Length; i++) {
            startPositions[i] = answers[i].transform.position;
            endPositions[i] = startPositions[i];
            endPositions[i].x -= 1050f;
        }

        SetInteractable(false);
        yield return new WaitForSeconds(outroDelay);

        float t = 0;
        while (t < 1f) {
            for (int i = 0; i < answers.Length; i++)
                answers[i].transform.position = Vector3.Lerp(startPositions[i], endPositions[i], t);
            t += outroSpeed * Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        for (int i = 0; i < answers.Length; i++)
            answers[i].transform.position = endPositions[i];

        callback?.Invoke();
    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);
        txtQuestion.gameObject.SetActive(value);

        foreach (GameObject answer in answers)
            answer.SetActive(value);
    }

    public void ShowAnswers() {
        StartCoroutine(IntroQuestion());
    }

    public void HideAnswers(Callback callback = null) {
        StartCoroutine(OutroQuestion(callback));
    }

    
}
