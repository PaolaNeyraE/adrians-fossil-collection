﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TestController : MonoBehaviour {

    [SerializeField]
    private int currentQuestion;
    [SerializeField]
    private int correctAnswers;


    public Question[] questions;

    [Header("Result")]
    public GameObject result;
    public GameObject winPanel;
    public GameObject losePanel;
    public TextMeshProUGUI txtResult;
    public GameObject glow;
    public GameObject bone;


    [Header("Loot")]
    public string collectionId;

    [Header("FX")]
    public AudioClip fxCorrect;
    public AudioClip fxIncorrect;
    public AudioClip fxBone;
    public AudioClip fxCounter;
    private AudioSource audioSource;


    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();

        SetActive(false);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }


    private IEnumerator ShowResultWin() {
        losePanel.SetActive(false);

        audioSource.PlayOneShot(fxBone);

        glow.transform.localScale = Vector3.zero;
        bone.transform.localScale = Vector3.zero;

        float t = 0;
        while (t < 1f) {
            glow.transform.localScale = Vector3.one * t;
            bone.transform.localScale = Vector3.one * t;

            t += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        glow.transform.localScale = Vector3.one;
        bone.transform.localScale = Vector3.one;


        while (true) {
            glow.transform.Rotate(Vector3.forward, 50f);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ShowResultTotalQuestion() {
        string text = txtResult.text;
        int corrects = 0;

        audioSource.PlayOneShot(fxCounter);
        while (corrects <= correctAnswers) {
            txtResult.text = text + " " + corrects + "/3";
            corrects++;

            //audioSource.PlayOneShot(fxCounter);

            yield return new WaitForSeconds(0.15f);
        }
    }


    public void SetActive(bool value) {
        foreach (Question question in questions)
            question.SetActive(value);

        result.SetActive(value);
    }

    public void ShowQuestion() {
        gameObject.SetActive(true);

        GameManager.PauseTimer();
        Player.Lock();
        Enemy.isLocked = true;


        if (currentQuestion > 0) {
            questions[currentQuestion - 1].HideAnswers(() => {
                questions[currentQuestion].SetActive(true);
                questions[currentQuestion].ShowAnswers();
            });
        }
        else {
            questions[currentQuestion].SetActive(true);
            questions[currentQuestion].ShowAnswers();
        }
    }

    public void ShowResult() {
        questions[currentQuestion - 1].HideAnswers(() => {
            foreach (Question question in questions)
                question.SetActive(false);

            result.SetActive(true);
            StartCoroutine(ShowResultTotalQuestion());


            if (correctAnswers >= questions.Length) {
                GameManager.AddCollection(collectionId);
                StartCoroutine(ShowResultWin());
            }
            else
                winPanel.SetActive(false);
        });
    }

    public void AnswerQuestion(bool value) {
        if (value) {
            correctAnswers++;
            PlayCorrect();
        }
        else {
            PlayIncorrect();
        }

        currentQuestion++;
        if (currentQuestion < questions.Length)
            ShowQuestion();
        else
            ShowResult();
    }

    public void PlayCorrect() {
        audioSource.PlayOneShot(fxCorrect);
    }

    public void PlayIncorrect() {
        audioSource.PlayOneShot(fxIncorrect);
    }
}
