﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelInfoController : MonoBehaviour {

    public TextMeshProUGUI txtMision;
    public SpeechManager speechManager;


    public string mision {
        set => txtMision.text = value;
    }



    // Start is called before the first frame update
    void Start() {
        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGMain();
        else
            GameManager.PlayBGOther();


        //if (value)
        //    mision = Level1_1Manager.currentMission;
    }

    public void SpeakMission() {
        speechManager.OnClickSpeakText("missions" + Level1_1Manager.currentMissionKey);
    }

    public void CancelSpeakMission() {
        speechManager.CancelText();
    }
}
