﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomizeController : MonoBehaviour {
    [SerializeField]
    private int maskSelected = -1;

    public MaskController maskController;
    public Button btnPlay;
    public GameObject message;
    public Image[] marks;

    // Start is called before the first frame update
    void Start() {
        HideMessage();
        SetActiveMask(false);
        //SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }

    private void ShowMessage() {
        message.SetActive(true);

        btnPlay.interactable = false;
    }

    private void HideMessage() {
        message.SetActive(false);

        btnPlay.interactable = true;
    }

    private void SetActiveMask(bool value) {
        foreach (Image mark in marks)
            mark.enabled = value;
    }

    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            //GameManager.PlayBGMain();
            GameManager.PlayBGOther();
        else
            //GameManager.PlayBGOther();
            GameManager.PlayBGMain();
    }

    public void SelectMask(int index) {
        SetActiveMask(false);

        maskSelected = (index != maskSelected) ? index : -1;

        if (maskSelected >= 0 && index < marks.Length)
            marks[index].enabled = true;

        maskController.SetActiveMaskByIndex(maskSelected);
        Player.SetMask(maskSelected);

        CancelInvoke();
        HideMessage();

        if (index >= 0)
            if (!CollectionsManager.CheckCollectionByIndex(maskSelected))
                ShowMessage();

    }

    public void Play() {

        //if (maskSelected >= 0) {
        //    if (CollectionsManager.CheckCollectionByIndex(maskSelected)) {
        //        AnimationDirector.StartIntro();
        //        SetActive(false);
        //    }
        //    else {
        //        SelectMask(-1);
        //        ShowMessage();
        //        Invoke(nameof(HideMessage), 5f);
        //    }
        //}
        //else {
        AnimationDirector.StartIntro();
        SetActive(false);
        //}
    }
}
