﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InspectorController : MonoBehaviour {

    static public InspectorController instance;

    static public void Show() {
        instance.SetActive(true);
    }

    static public void Hide() {
        instance.SetActive(false);
    }

    static public void FillData(ItemData item) {
        instance.SetData(item);
    }



    public Image image;
    public TextMeshProUGUI txtName;
    public TextMeshProUGUI txtInfo1;
    public TextMeshProUGUI txtInfo2;

    // Start is called before the first frame update
    void Start() {
        instance = this;
        SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        if (value)
            GameManager.PlayBGMain();
        else
            GameManager.PlayBGOther();
    }

    public void SetData(ItemData item) {
        image.sprite = item.sprite;
        txtName.text = item.name;
        txtInfo1.text = item.info1;
        txtInfo2.text = item.info2;
    }

    public void Next() {
        Inventory.NextFossil();
    }

    public void Previous() {
        Inventory.PrevFossil();
    }

}
