﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour {

    private float closePos = -1.8f;
    private float openPos = -4.18f;
    private float t = 0;

    private Coroutine move;


    public Transform gate;
    public float speed = 2;

    // Start is called before the first frame update
    void Start() {
        gate.localPosition = Vector3.up * openPos;
    }

    // Update is called once per frame
    void Update() {

    }


    private IEnumerator Move(float target) {
        Vector3 from = gate.localPosition;
        Vector3 to = Vector3.up * target;

        while (t <= 1f) {
            gate.localPosition = Vector3.Lerp(from, to, t);

            t += Time.deltaTime * speed;

            yield return new WaitForEndOfFrame();
        }

        t = 0;
        gate.localPosition = to;

    }

    private void OpenClose(float target) {
        if (move != null)
            StopCoroutine(move);

        move = StartCoroutine(Move(target));
    }


    public void Open() {
        Collider2D[] colliders = gate.GetComponents<Collider2D>();
        foreach (Collider2D collider in colliders)
            collider.enabled = false;

        OpenClose(openPos);
    }

    public void Close() {
        Collider2D[] colliders = gate.GetComponents<Collider2D>();
        foreach (Collider2D collider in colliders)
            collider.enabled = true;

        OpenClose(closePos);
    }


}
