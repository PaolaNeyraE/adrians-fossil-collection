﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Collection : MonoBehaviour {


    public TextMeshProUGUI txtProgress;

    public GameObject[] parts;


    public Vector3 position {
        get => transform.position;
        set => transform.position = value;
    }

    public bool isComplete {
        get {
            int count = 0;
            foreach (GameObject part in parts)
                if (part.activeSelf)
                    count++;

            return count >= parts.Length;
        }
    }


    void Awake() {
        foreach (GameObject part in parts)
            part.SetActive(false);
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }


    public void SetActivePart(int index, bool value) {
        parts[index].SetActive(value);

        int count = 0;
        foreach (GameObject part in parts)
            if (part.activeSelf)
                count++;

        txtProgress.text = count + "/" + parts.Length;
    }

    public bool[] Export() {
        bool[] export = new bool[parts.Length];
        
        for (int i = 0; i < parts.Length; i++)
            export[i] = parts[i].activeSelf;

        return export;
    }
}
