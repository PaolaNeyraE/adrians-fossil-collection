﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cure : MonoBehaviour {

    public AudioClip clip;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnDestroy() {
        GameObject fx = new GameObject("FX");
        AudioSource audio = fx.AddComponent<AudioSource>();
        audio.clip = clip;
        audio.playOnAwake = false;
        audio.loop = false;
        audio.Play();

        Destroy(fx, clip.length);
    }
}
