﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class ItemsDB : MonoBehaviour {

    static private ItemsDB instance;

    static public ItemData GetItemDataById(int id) {
        ItemData itemData = instance.items.Find(item => item.id == id);
        itemData.name = SharedState.LanguageDefs["fossilsf" + itemData.id + "name"];
        itemData.info1 = SharedState.LanguageDefs["fossilsf" + itemData.id + "descriptionLeft"];
        itemData.info2 = SharedState.LanguageDefs["fossilsf" + itemData.id + "descriptionRight"];

        return itemData;
    }

    static public void LoadFossilsInfo(JSONNode data) {
        instance.LoadInfo(data);
    }

    public List<ItemData> items;

    // Start is called before the first frame update
    void Start() {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update() {

    }

    private void LoadInfo(JSONNode data) {
        items.ForEach(item => {
            item.name = data["f" + item.id]["name"];
            item.info1 = data["f" + item.id]["descriptionLeft"];
            item.info2 = data["f" + item.id]["descriptionRight"];
        });

    }
}
