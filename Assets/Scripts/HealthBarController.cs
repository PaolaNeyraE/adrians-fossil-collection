﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBarController : MonoBehaviour {

    static private HealthBarController instance;

    static public void SetValue(int value) {
        instance.slider.value = value / (float)instance.maxValue;

        instance.txtHP.text = value + "/" + instance.maxValue;
    }

    static public void SetMaxValue(int value) {
        instance.maxValue = value;
    }


    private int maxValue;

    public Slider slider;
    public TextMeshProUGUI txtHP;



    void Awake() {
        instance = this;

        slider.value = 1f;
        maxValue = 9;
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
