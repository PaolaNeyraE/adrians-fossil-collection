﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CollectionsController : MonoBehaviour {

    static private CollectionsController instance;

    static public void UpdateDescription(string description) {
        instance.txtDescription.text = description;
    }

    static public void HideDescription() {
        instance.txtDescription.transform.parent.gameObject.SetActive(false);
    }

    static public void ShowDescription() {
        instance.txtDescription.transform.parent.gameObject.SetActive(true);
    }


    public TextMeshProUGUI txtDescription;
    public TextMeshProUGUI[] texts;


    void Awake() {
        instance = this;
    }

    // Start is called before the first frame update
    void Start() {
        //instance = this;

        SetActive(false);
        UpdateTexts();
    }

    // Update is called once per frame
    void Update() {

    }


    public void SetActive(bool value) {
        gameObject.SetActive(value);

        //if (value)
        //    GameManager.PlayBGMain();
        //else
        //    GameManager.PlayBGOther();
    }

    public void UpdateTexts() {
        foreach (TextMeshProUGUI text in texts) {
            string[] cmds = text.text.Split('+');
            //text.text = SharedState.LanguageDefs["collectionsPanel"][cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
            text.text = SharedState.LanguageDefs["collectionsPanel"+cmds[0]] + (cmds.Length == 2 ? cmds[1].Replace("\"", "") : "");
        }

        UpdateDescription(SharedState.LanguageDefs["collectionstrex"]);

    }
}
