﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskController : MonoBehaviour {

    public GameObject[] masks;

    // Start is called before the first frame update
    void Start() {
        SetActiveMaskByIndex();
    }

    // Update is called once per frame
    void Update() {

    }

    public void SetActiveMaskByIndex(int index = -1) {
        foreach (GameObject mask in masks)
            mask.SetActive(false);

        if (index >= 0 && index < masks.Length)
            masks[index].SetActive(true);
    }
}
